//
// Created by Егор on 08.01.2023.
//

#ifndef IMAGE_TRANSFORMER_FILEWORK_H
#define IMAGE_TRANSFORMER_FILEWORK_H

#include <stdbool.h>
#include <stdio.h>

enum err_s {
    OPEN_OK = 0,
    CLOSE_OK,
    OPEN_ERROR,
    CLOSE_ERROR,
    INCORRECT_FILE_NAME,
    CLOSED_FILE_ERROR
};

static const char* get_err_string(enum err_s);

enum err_s of_read(const char *file_name, FILE **file);

enum err_s of_write(const char *file_name, FILE **file);

enum err_s file_close(FILE **file);

static void logger( const char * str);

#endif //IMAGE_TRANSFORMER_FILEWORK_H

