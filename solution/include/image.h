//
// Created by Егор on 09.01.2023.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "mm_malloc.h"
#include "stdbool.h"
#include "stdint.h"

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};


struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width;
    uint32_t height;
    struct pixel * data;
};

struct image create_image (uint32_t width, uint32_t height);
void destroy_img (struct image * img);
bool set_pixel (const struct image * image, struct pixel pixel, uint32_t row, uint32_t column);
struct pixel get_pixel (const struct image * image, uint32_t height, uint32_t width);

#endif //IMAGE_TRANSFORMER_IMAGE_H
