//
// Created by Егор on 09.01.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include "stdio.h"
#include "stdint.h"
#include <stdbool.h>

enum read_status from_bmp(FILE * in, struct image * image );
enum write_status to_bmp(FILE * output_file, struct image const * image );

#endif //IMAGE_TRANSFORMER_BMP_H
