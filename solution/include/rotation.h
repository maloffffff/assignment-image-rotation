//
// Created by Егор on 11.01.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H
struct image rotate(const struct image* source_image);
#endif //IMAGE_TRANSFORMER_ROTATION_H
