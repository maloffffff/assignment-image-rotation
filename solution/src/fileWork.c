//
// Created by Егор on 08.01.2023.
//

#include "fileWork.h"

static const char *err_string[] = {
        [OPEN_OK] =             "File was successfully opened",
        [CLOSE_OK] =            "File was successfully closed",
        [OPEN_ERROR]=           "Error with file opening",
        [CLOSE_ERROR] =         "Error with file closing",
        [INCORRECT_FILE_NAME]=  "Error, file name cannot be null",
        [CLOSED_FILE_ERROR]=    "Error, can't work with closed file"

};

static enum err_s open_file_mode(const char *filename, FILE **fp, const char *m) {
    if (!filename)
        return INCORRECT_FILE_NAME;
    *fp = fopen(filename, m);
    if (!*fp)
        return OPEN_ERROR;
    return OPEN_OK;
}

const char * get_err_string(enum err_s string) {
    return err_string[string];
}

static void logger(const char *str){
    fprintf(stderr,"%s", str);
}

enum err_s of_read(const char *file_name, FILE **file) {
    const char *mode = "r";
    return open_file_mode(file_name, file, mode);
}

enum err_s of_write(const char *file_name, FILE **file) {
    const char *mode = "w";
    return open_file_mode(file_name, file, mode);
}

enum err_s file_close(FILE **file) {
    if (!*file)
        return CLOSED_FILE_ERROR;
    if (fclose(*file))
        return CLOSE_ERROR;
    return CLOSE_OK;
}
