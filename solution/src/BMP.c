//
// Created by Егор on 09.01.2023.
//

#include "BMP.h"

static const uint32_t BICOMPRESSION = 0;
static const uint32_t BIXPELSPERMETER = 2834;
static const uint32_t BIYPELSPERMETER = 2834;
static const uint32_t BICLRUSED = 0;
static const uint32_t BICLRIMPORTANT = 0;
static const uint16_t BIPLANES = 1;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool is_bmp_header (struct bmp_header header) {
    return header.bfType == 0x4D42;
}

static bool add_padding (const struct image * image) {
    return (image->width * 3) % 4;
}

static uint32_t calc_padding (const struct image * image) {
    return (4 - ((image->width * 3) % 4));
}

static bool write_padding (FILE * out, const struct image * image) {
    int8_t z = 0;
    for (size_t i = 0; i < calc_padding(image); i++) {
        if (!fwrite(&z, sizeof (int8_t), 1, out)) return false;
    }
    return true;
}

static uint32_t file_size_calc (const struct image * image) {
    return sizeof (struct bmp_header) + image->width * image->height * sizeof(struct pixel)
           + image->height * calc_padding(image);
}

static bool to_image (FILE * file, const struct bmp_header header, struct image * img) {
    *img = create_image(header.biWidth, header.biHeight);
    fseek(file, header.bOffBits, SEEK_SET);
    for (uint32_t i = 0; i < img->height; i++) {
        for (uint32_t j = 0; j < img->width; j++) {
            struct pixel px = {0};
            if (!fread(&px, sizeof(struct pixel), 1, file)) {
                destroy_img(img);
                return false;
            }
            set_pixel(img, px, i, j);
        }
        if (add_padding(img)) fseek(file, calc_padding(img), SEEK_CUR);
    }
    return true;
}

static struct bmp_header create_bmp_header (const struct image * image) {
    return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = file_size_calc(image),
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BIPLANES,
            .biBitCount = 24,
            .biCompression = BICOMPRESSION,
            .biSizeImage = file_size_calc(image) - 54,
            .biXPelsPerMeter = BIXPELSPERMETER,
            .biYPelsPerMeter = BIYPELSPERMETER,
            .biClrUsed = BICLRUSED,
            .biClrImportant = BICLRIMPORTANT,
    };
}

enum read_status from_bmp (FILE * input_file, struct image * image) {
    if (!input_file) return READ_ERROR;
    struct bmp_header header = {0};
    if (!fread(&header, sizeof (struct bmp_header), 1, input_file)) return READ_INVALID_HEADER;
    if (!is_bmp_header(header)) return READ_INVALID_SIGNATURE;
    if (!to_image(input_file, header, image)) return READ_INVALID_BITS;
    return READ_OK;
}



enum write_status to_bmp (FILE * output_file, const struct image * image) {
    if (!output_file) return WRITE_ERROR;
    struct bmp_header header = create_bmp_header(image);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, output_file)) return WRITE_ERROR;
    for (uint32_t i = 0; i < header.biHeight; i++) {
        for (uint32_t j = 0; j < header.biWidth; j++) {
            struct pixel pixel = get_pixel(image, i, j);
            if (!fwrite(&pixel, sizeof (struct pixel), 1, output_file)) return WRITE_ERROR;
        }
        if (add_padding(image)) {
            if (!write_padding(output_file, image)) return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
