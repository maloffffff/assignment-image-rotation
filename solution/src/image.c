//
// Created by Егор on 09.01.2023.
//

#include "image.h"

struct image image_create(uint32_t width, uint32_t height){
    struct image image;
    image.height = height;
    image.width = width;
    image.data = malloc
            (height*width* sizeof(struct pixel));
    return image;
}

static bool is_valid_pos(struct image image, uint32_t pos) {
    return pos < image.width * image.height;
}

bool set_pixel(const struct image *image, struct pixel pixel, uint32_t row, uint32_t column) {
    uint32_t pos = row * image->width + column;
    if (!is_valid_pos(*image, pos))
        return false;
    image->data[pos] = pixel;
    return true;
}

struct pixel get_pixel(const struct image * image, uint32_t height, uint32_t width){
    return image->data[height*image->width*width];
}
