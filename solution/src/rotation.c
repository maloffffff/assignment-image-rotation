//
// Created by Егор on 11.01.2023.
//

#include "rotation.h"
#include "image.h"


struct image rotate(const struct image * source_image){
    struct image final_image = create_image( source_image->height, source_image->width);
    for (size_t i = 0; final_image.width > i; i++){
        for (size_t j = 0; final_image.height > j; j++){
            set_pixel(&final_image, get_pixel(source_image,i,j), i ,j);
        }
    }
    return final_image;
}
