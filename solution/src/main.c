#include <stdio.h>
#include "../../tester/include/image.h"
#include "fileWork.h"
#include "image.h"
#include "BMP.h"
#include "rotation.h"

int main( int argc, char** argv ) {
    if (argc != 3) fprintf(stderr,"Incorrect number of arguments\n");
    FILE * source_file;
    FILE * finish_file;
    struct image image;
    const char *const source_filename = argv[1];
    const char *const finish_filename = argv[2];
    enum err_s s = of_read(source_filename, &source_file);
    if (s != OPEN_OK) return OPEN_ERROR;

    logger(get_err_string(s));
    enum read_status readStatus = from_bmp(source_file, &image);
    if (readStatus != READ_OK) return READ_ERROR;
    struct image final_image = rotate(&image);

    switch(from_bmp(source_file,&image)){
        case(READ_INVALID_HEADER):{
            fprintf(stderr,"Invalid header reading");
            file_close(&source_file);
        }
        case(READ_INVALID_BITS):{
            fprintf(stderr,"Invalid bits reading");
            file_close(&source_file);
        }
        case(READ_INVALID_SIGNATURE):{
            fprintf(stderr,"Invalid signature reading");
            file_close(&source_file);
        }
        case(READ_ERROR):{
            fprintf(stderr,"Cannot open the file");
            file_close(&source_file);
        }
        case(READ_OK):
            break;
    }
    if(file_close(&source_file) != CLOSE_OK){
        fprintf(stderr,"File closing error");
        free((&image)->data);
        return 1;
    }
    to_bmp(finish_file,&final_image);
    free((&image)->data);
    free((&final_image)->data);
    return 0;
}
